# Dotfiels #

### What is this repository for? ###

* Dotfile repository
* Version: 1.2

### Contains ###

* inputrc
* vimrc and more vim files
* bashrc and more bash files
* eslintrc (js)
* example gitignore file

### Setup ###

git clone git@bitbucket.org:qlimax/dotfiles.git && cd dotfiles && ./install

if ubuntu terminal should use onedark color schema:

`cd bash/one-gnome-terminal; chmod -x one-dark.sh; ./one-dark.sh`
-> Change profile in Terminal.

### Developer ###

* Daniel Supplieth
* daniel.supplieth@gmx.de

